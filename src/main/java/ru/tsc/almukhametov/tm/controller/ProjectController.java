package ru.tsc.almukhametov.tm.controller;

import ru.tsc.almukhametov.tm.api.Controller.IProjectController;
import ru.tsc.almukhametov.tm.api.service.IProjectService;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[ClEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProjects() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

}
