package ru.tsc.almukhametov.tm.api.Controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProjects();

}
