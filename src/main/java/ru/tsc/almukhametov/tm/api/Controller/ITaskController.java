package ru.tsc.almukhametov.tm.api.Controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTasks();

}
